from abc import ABC, abstractmethod

class Animal(ABC):
	def eat(self, food):
		pass

	def make_sound(self):
		pass


class Dog(Animal):
	def __init__(self, name, breed, age):
		super().__init__()
		self._name = name
		self._breed = breed
		self._age = age

	def get_name(self):
		return self._breed

	def get_breed(self):
		return self._age

	def get_age(self):
		return self._name
 
         
	def set_name(self):
		self._name = name

	def set_name(self):
		self._breed = breed

	def set_name(self):
		self._age = age



	def eat(self, food):
		print(f"Eaten {food}")

	def make_sound(self):
		print("Bark!")

	def call(self):
		print(f"Here {self._name}")


class Cat(Animal):
	def __init__(self, name, breed, age):
		super().__init__()
		self._name = name
		self._breed = breed
		self._age = age

	def get_name(self):
		return self._breed

	def get_breed(self):
		return self._age

	def get_age(self):
		return self._name
 
         
	def set_name(self):
		self._name = name

	def set_name(self):
		self._breed = breed

	def set_name(self):
		self._age = age



	def eat(self, food):
		print(f"Eaten {food}")

	def make_sound(self):
		print("Arf!")

	def call(self):
		print(f"Here {self._name}")


class Cat(Animal):
	def __init__(self, name, breed, age):
		super().__init__()
		self._name = name
		self._breed = breed
		self._age = age

	def get_name(self):
		return self._breed

	def get_breed(self):
		return self._age

	def get_age(self):
		return self._name
 
         
	def set_name(self):
		self._name = name

	def set_name(self):
		self._breed = breed

	def set_name(self):
		self._age = age



	def eat(self, food):
		print(f"Serve me {food}")

	def make_sound(self):
		print("Meow!")

	def call(self):
		print(f"{self._name} come on!")

dog1 = Dog("Isis", "Dalmatian", 15)
dog1.eat("Steak")
dog1.make_sound()
dog1.call()


cat1 = Cat("Puss", "Persian", 4)
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()